import jsPlaceholder from '../api/jsonPlaceholder';
import _ from 'lodash';
export const fetchPosts =  () => {
    return async (dispatch)=> {
        const response = await jsPlaceholder.get('/posts');

        dispatch({
            type: 'FETCH_POSTS',
            payload: response.data
        });
    }
};


export const fetchUser =  (user_id) => {
    return async (dispatch)=> {
        const response = await jsPlaceholder.get(`/users/${user_id}`);

        dispatch({
            type: 'FETCH_USER',
            payload: response.data
        });
    }
};


export const fetchPostsAndUsers = () => {
    return async (dispatch,getState)=>{ 
        await dispatch(fetchPosts());
      

        const userIds=_.uniq(_.map(getState().postsReducer,'userId'));
        userIds.forEach((id)=>{
            dispatch(fetchUser(id));
        })
    }
};
